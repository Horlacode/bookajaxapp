from django.conf.urls import url
from django.contrib import admin
from bookajax import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.book_list, name='book_list'),
    url(r'^crud/(?P<status>[^/]+)/$', views.crud, name='crud'),
]
