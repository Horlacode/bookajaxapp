from django.apps import AppConfig


class BookajaxConfig(AppConfig):
    name = 'bookajax'
