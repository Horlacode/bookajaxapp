from django.contrib import messages
from django.shortcuts import render, redirect, get_object_or_404
from django.http import JsonResponse
from django.template.loader import render_to_string
from .models import Book
from .forms import BookForm


# Create your views here.


def book_list(request):
    template_name = 'bookajax/index.html'
    return render(request, template_name, locals())


def crud(request, status):
    context = {}
    if request.method == 'GET':
        if status == 'get_book_list':
            books = Book.objects.all()
            context['template'] = render_to_string('bookajax/book_list.html', {'books': books})
            return JsonResponse(context, status=200, safe=False)
        elif status == 'get_book_form':
            form = BookForm()
            context['template'] = render_to_string('bookajax/book_create.html', {'form': form})
            return JsonResponse(context, status=200, safe=False)
        elif status == 'get_book_edit_form':
            form = BookForm(instance=Book.objects.get(pk=request.GET.get('pk')))
            context['template'] = render_to_string('bookajax/book_create.html', {'form': form})
            return JsonResponse(context, status=200, safe=False)
    else:
        if status == 'save':
            form = BookForm(request.POST or None)
            if form.is_valid():
                form.save()
                context['status'] = 'success'
                context['message'] = 'Book successfully created'
            else:
                print(form.errors)
                context['status'] = 'error'
                context['message'] = 'Something went wrong while creating this book'
            return JsonResponse(context, status=200, safe=False)


# not using this one
def book_ajax_create_demo(request):
    # data = dict()
    template_name = 'bookajax/book_create.html'
    if request.method == 'POST':
        form = BookForm(request.POST or None)
        if form.is_valid():
            form.save()
            messages.success(request, f'Book is Created')
            return redirect('book_list')
    else:
        form = BookForm()
    context = {'form': form}
    return render(request, template_name, context)


def book_ajax_create(request):
    data = dict()
    template_name = 'bookajax/book_create.html'
    if request.method == 'GET':
        form = BookForm()
        context = {'form': form}
        return render(request, template_name, context)
        # data['html_form'] = render_to_string(template_name, context, request=request)
        # return JsonResponse(data)
    elif request.method == 'POST':
        form = BookForm(request.POST or None)
        if form.is_valid():
            form.save()
            data['html_form_status'] = True
            books = Book.objects.all()
            context = {'books': books}
            data['html_form_mssg'] = messages.warning(request, f'Book is created')
            data['html_form_data'] = render_to_string('bookajax/book_list.html', context)
        else:
            data['html_form_status'] = False
        context = {'form': form}
        data['html_form'] = render_to_string(template_name, context)
        return JsonResponse(data, status=200, safe=False)


# Edit and delete functions not converted to ajax yet ..

def book_ajax_update(request, pk):
    template_name = 'bookajax/book_update.html'
    book = get_object_or_404(Book, id=pk)
    if request.method == 'POST':
        form = BookForm(request.POST, instance=book)
        if form.is_valid():
            form.save()
            messages.success(request, f' Book is updated ')
            return redirect('book_list')
    else:
        form = BookForm(instance=book)
    context = {"form": form}
    return render(request, template_name, context)


def book_ajax_delete(request, pk):
    book = get_object_or_404(Book, pk=pk)
    book.delete()
    messages.warning(request, f'Book {book.title} is deleted')
    return redirect('book_list')
