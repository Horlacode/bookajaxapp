$(document).ready(function () {
    getBook('/crud/get_book_list');
});

function getBook(url) {
    $.get(url, {}, function () {
    }).done(function (response) {
        $('.app-book-holder').html(response.template);
         $('#id_publication_date').datepicker({
            format: "yyyy-mm-dd"
        });
    }).fail(function (error) {

    })

}
function getId(pk){
    alert(pk);
}
$('.app-book-holder').on('submit', '#js_book_create', function (event) {
    event.preventDefault();
    var data = $('#js_book_create').serialize() + '&csrfmiddlewaretoken=%csrf%'.replace('%csrf%', $('input[name=csrfmiddlewaretoken]').val());
    $.post('/crud/save/', data, function (ve) {

    }).done(function (response) {
        if (response.status === 'success') {
            getBook('/crud/get_book_list');
        } else {
            alert(response.message);
        }
    }).fail(function (error) {
        var html = " <div class=\"alert alert-danger alert-dismissible fade show\" role=\"alert\">\n" +
            "                                        <i class=\"mdi mdi-block-helper mr-2\"></i> Something went wrong while performing\n" +
            "                                        this action :)\n" +
            "                                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n" +
            "                                            <span aria-hidden=\"true\">&times;</span>\n" +
            "                                        </button>\n" +
            "                                    </div>";
        $('#fds-alert-holder').html(html);
    })

})
